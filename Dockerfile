From nginx:1.20.0

ENV http_proxy="http://fastweb.bell.ca:8083"
ENV https_proxy="http://fastweb.bell.ca:8083"
ENV no_proxy="127.0.0.1,localhost,.bell.ca,.bce.ca,*.bell.ca,*.bce.ca,*.local, 169.254/16, 1, riksrv.redirectme.net, 10.*, aide502-v01, *.uat.int.bell.ca, 172.24.204.154, 142.117.244.146, 142.182.11.103, 142.122.5.17, 142.117.244.156, aial2132.belldev.dev.bce.ca, belleclf-local, sslvpnqr.bell.ca, 198.235.67.241, 198.235.67.243, 10.199.1.51, 142.182.19.14, 10.55.66.202, ews.exchange.bell.ca, *.enterprisecentre.ms.bell.ca, *.it.ms.bell.ca, *.vpn.bell.ca, oc-local, osc-consolidator, oc-master.lab.enterprisecentre.ms.bell.ca, apis.qa.ms.bell.ca, apis.int.ms.bell.ca, paco.int.ms.bell.ca, *.wnst.int.bell.ca, service-local, api.uat.operationcentre.ms.bell.ca"

RUN chmod g+rwx /var/cache/nginx /var/run /var/log/nginx
#RUN cp /etc/nginx/nginx.conf /etc/nginx/nginx.conf.bkp
#COPY nginx.conf /etc/nginx/nginx.conf

RUN sed -i.bak 's/^user/#user/' /etc/nginx/nginx.conf